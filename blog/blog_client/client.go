package main

import (
	"context"
	"fmt"
	"grpc-go/blog/blogpb"
	"io"
	"log"

	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Blog Client function")

	opts := grpc.WithInsecure()
	cc, err := grpc.Dial("localhost:50051", opts)
	if err != nil {
		log.Fatalf("could not connect %v", err)
	}
	defer cc.Close()

	c := blogpb.NewBlogServiceClient(cc)

	fmt.Println("Creating the blog")
	blog := blogpb.Blog{
		AuthorId: "Mehari",
		Title:    "My Fist Blog",
		Content:  "My Fist Blog Content",
	}
	createBlogRes, err := c.CreateBlog(context.Background(), &blogpb.CreateBlogRequest{Blog: &blog})
	if err != nil {
		log.Fatalf("Unexpected error: %v", err)
	}

	fmt.Printf("Blog has been created: %v \n", createBlogRes)

	// read blog
	fmt.Println("reading the blog")

	blogId := "02045108-1303-11ed-b470-705a0f1be42f"
	readBlogReq := &blogpb.ReadBlogRequest{BlogId: blogId}
	readBlogRes, readBogErr := c.ReadBlog(context.Background(), readBlogReq)
	if readBogErr != nil {
		log.Printf("Error happened while reading: %v", readBogErr)
		return
	}

	fmt.Printf("Blog was read: %v \n", readBlogRes)

	// update blog
	newBlog := blogpb.Blog{
		Id:       &blogpb.UUID{Value: blogId},
		AuthorId: "Changed Author",
		Title:    "Edited Fist Blog",
		Content:  "Some awesome Fist Blog Content",
	}

	updateRes, updateErr := c.UpdateBlog(context.Background(), &blogpb.UpdateBlogRequest{Blog: &newBlog})
	if updateErr != nil {
		fmt.Printf("Error while updating: %v \n", updateErr)
		return
	}

	fmt.Printf("Blog was updated: %v \n", updateRes)

	// delete blog
	deleteRes, deleteErr := c.DeleteBlog(context.Background(), &blogpb.DeleteBlogRequest{BlogId: blogId})
	if deleteErr != nil {
		fmt.Printf("Error while deleting: %v \n", deleteErr)
		return
	}

	fmt.Printf("Blog was Deleted: %v \n", deleteRes)

	// list Blogs
	stream, err := c.ListBlog(context.Background(), &blogpb.ListBlogRequest{})

	if err != nil {
		log.Fatalf("error while calling ListBlog RPC: %v", err)
	}

	for {
		res, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Something happened while server streaming: %v", err)
		}

		fmt.Println(res.GetBlog())
	}
}
