package main

import (
	"context"
	"fmt"
	"grpc-go/blog/blogpb"
	"log"
	"net"
	"os"
	"os/signal"

	"github.com/jackc/pgx/v4"
	uuid "github.com/satori/go.uuid"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
)

var Db *pgx.Conn

type server struct {
	blogpb.UnimplementedBlogServiceServer
}

type blogItem struct {
	Id       uuid.UUID `json:"id" gorm:"primary_key;"`
	AuthorId string    `json:"author_id,omitempty"`
	Content  string    `json:"content,omitempty"`
	Title    string    `json:"title,omitempty"`
}

func (*server) CreateBlog(ctx context.Context, req *blogpb.CreateBlogRequest) (*blogpb.CreateBlogResponse, error) {
	blog := req.GetBlog()

	fmt.Println("create blog request")
	data := blogItem{
		AuthorId: blog.AuthorId,
		Title:    blog.Title,
		Content:  blog.Content,
	}

	id := uuid.NewV1()

	sqlStatement := `INSERT INTO blogs (id, author_id, title,  content) VALUES ($1, $2, $3, $4)`

	_, err := Db.Exec(context.Background(), sqlStatement, id.String(), data.AuthorId, data.Title, data.Content)
	if err != nil {
		log.Fatalf("Database error %v", err)
		return nil, err
	}

	return &blogpb.CreateBlogResponse{
		Blog: &blogpb.Blog{
			Id:       &blogpb.UUID{Value: id.String()},
			Title:    data.Title,
			AuthorId: data.AuthorId,
			Content:  data.Content,
		},
	}, nil
}

func (*server) ReadBlog(ctx context.Context, req *blogpb.ReadBlogRequest) (*blogpb.ReadBlogResponse, error) {
	fmt.Println("Read Blog request")

	blogID := req.GetBlogId()

	var data blogItem

	res := Db.QueryRow(context.Background(), fmt.Sprintf("SELECT * FROM blogs WHERE id='%s'", blogID))
	err := res.Scan(&data.Id, &data.Title, &data.AuthorId, &data.Content)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("cannot find object in database: %v", err),
		)
	}

	return &blogpb.ReadBlogResponse{
		Blog: dataToBlogPb(&data),
	}, nil
}

func (*server) ListBlog(req *blogpb.ListBlogRequest, stream blogpb.BlogService_ListBlogServer) error {
	fmt.Println("List Blog request")

	var blog blogItem

	sqlStatement := `SELECT * FROM blogs`
	rows, err := Db.Query(context.Background(), sqlStatement)
	if err != nil {
		return status.Errorf(
			codes.Internal,
			fmt.Sprintf("unknown error: %v", err),
		)
	}

	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&blog.Id, &blog.AuthorId, &blog.Title, &blog.Content)
		if err != nil {
			return status.Errorf(
				codes.Internal,
				fmt.Sprintf("Decoding data error from database: %v", err),
			)
		}

		stream.Send(&blogpb.ListBlogResponse{
			Blog: dataToBlogPb(&blog),
		})
	}

	return nil
}

func dataToBlogPb(data *blogItem) *blogpb.Blog {
	return &blogpb.Blog{
		Id:       &blogpb.UUID{Value: data.Id.String()},
		Title:    data.Title,
		AuthorId: data.AuthorId,
		Content:  data.Content,
	}
}

func (*server) UpdateBlog(ctx context.Context, req *blogpb.UpdateBlogRequest) (*blogpb.UpdateBlogResponse, error) {
	fmt.Println("Update Blog request")

	blog := req.GetBlog()
	blogId := blog.Id.Value

	data := blogItem{}

	res := Db.QueryRow(context.Background(), fmt.Sprintf("SELECT * FROM blogs WHERE id='%s'", blogId))
	err := res.Scan(&data.Id, &data.Title, &data.AuthorId, &data.Content)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("cannot update object in database: %v\n", err),
		)
	}

	// we update our internal struct
	data.AuthorId = blog.GetAuthorId()
	data.Content = blog.GetContent()
	data.Title = blog.GetTitle()

	sqlStatement := fmt.Sprintf("UPDATE blogs SET id=$1, author_id=$2, title=$3, content=$4 WHERE id='%s'", blogId)
	_, err = Db.Exec(context.Background(), sqlStatement, blogId, data.AuthorId, data.Content, data.Title)

	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("cannot update object in database: %v\n", err),
		)
	}

	return &blogpb.UpdateBlogResponse{
		Blog: dataToBlogPb(&data),
	}, nil
}

func (*server) DeleteBlog(ctx context.Context, req *blogpb.DeleteBlogRequest) (*blogpb.DeleteBlogResponse, error) {
	fmt.Println("Delete Blog request")

	blogId := req.GetBlogId()

	sqlStatement := "DELETE FROM blogs WHERE id=$1"
	res, err := Db.Exec(context.Background(), sqlStatement, blogId)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("Cannot delete object in database: %v\n", err),
		)
	}

	if res.RowsAffected() == 0 {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("Cannot not find object in database: %v\n", err),
		)
	}

	return &blogpb.DeleteBlogResponse{BlogId: blogId}, nil
}

func main() {
	// if we crash the go code, we get the file name and line number
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	fmt.Println("Connecting to Database")

	connString := "postgresql://root@localhost:26257/grpc?sslmode=disable"
	config, err := pgx.ParseConfig(connString)
	config.Database = "grpc"
	if err != nil {
		log.Fatal("error configuring the database: ", err)
	}
	conn, err := pgx.ConnectConfig(context.Background(), config)
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	Db = conn

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	fmt.Println("Blog Service started")

	opts := []grpc.ServerOption{}
	s := grpc.NewServer(opts...)
	blogpb.RegisterBlogServiceServer(s, &server{})

	// Register reflection service on gRPC server.
	reflection.Register(s)

	go func() {
		fmt.Println("starting Server")
		if err := s.Serve(lis); err != nil {
			log.Fatalf("failed to server: %v", err)
		}
	}()

	// Wait for Control C to exit
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	// Block until a signal is received
	<-ch

	fmt.Println("Stopping the server")

	s.Stop()
	fmt.Println("stopping the listener")

	lis.Close()

	fmt.Println("Closing db connection")

	fmt.Println("End program")
}
